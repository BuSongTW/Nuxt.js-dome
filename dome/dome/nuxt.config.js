module.exports = {
  /*
  ** Headers of the page
  */

 head() {
  return {
    title: '我的藏书阁_书友最值得收藏的网络小说阅读网!',
    meta: [
      {
        hid: 'description',
        name: 'description',
        content:
          '我的藏书阁是广大书友最值得收藏的网络小说阅读网，网站收录了当前最火热的网络小说，免费提供高质量的小说最新章节，是广大网络小说爱好者必备的小说阅读网。'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: '我的藏书阁,小说阅读网,'
      }
    ]
  }
},
  css:['~assets/css/common.css'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

